<?php

namespace App\Controller;

use App\Service\RecuperationDeDonnees;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class QuiSommesNousController extends AbstractController
{
    public function __construct(
        private RecuperationDeDonnees $recuperationDeDonnees
    ) {
    }

    #[Route('/qui/sommes/nous', name: 'app_qui_sommes_nous')]
    public function index(): Response
    {
        $images = $this->recuperationDeDonnees;
        $imageHistorique = $images->qui()['historique'];
        $imageOrganigramme = $images->qui()['organigramme'];

        return $this->render('qui_sommes_nous/index.html.twig', [
            'images' => $images,
            'imageHistorique' => $imageHistorique,
            'imageOrganigramme' => $imageOrganigramme,
        ]);
    }
}
