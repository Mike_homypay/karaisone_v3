<?php

namespace App\Controller;

use App\Repository\AlbumsRepository;
use App\Repository\PicturesRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RealisationsController extends AbstractController
{
    #[Route('/realisations', name: 'app_realisations')]
    public function index(AlbumsRepository $albums): Response
    {
        $album = $albums->findAll();

        return $this->render('realisations/index.html.twig', [
            'albums' => $album,
        ]);
    }

    #[Route('realisations/{id}/images', name: 'app_images')]
    public function imagesAlbums(PicturesRepository $pictures, int $id)
    {

        $images = $pictures->findBy(['albums' => $id]);


        return $this->render('realisations/images.html.twig', [
            'images' => $images
        ]);
    }
}
