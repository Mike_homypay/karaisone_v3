<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Service\FormService;
use Doctrine\ORM\EntityManager;
use App\Service\RecuperationDeDonnees;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    public function __construct(
        private RecuperationDeDonnees $recuperationDeDonnees,
        private FormService $formService
    ) {
    }

    #[Route('/contact', name: 'app_contact')]
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {

        $headers = $this->recuperationDeDonnees->contact()['headers'];

        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contact
                ->setCreatedAt(new \DateTimeImmutable('now'))
                ->setSujet($form->getData()->getSujet())
                ->setNom($form->getData()->getNom())
                ->setEmail($form->getData()->getEmail())
                ->setTelephone($form->getData()->getTelephone())
                ->setMessage($form->getData()->getMessage())
                ->setFichier($form->getData()->getFile());

            $entityManager->persist($contact);
            $entityManager->flush();

            $this->addFlash('success', 'Votre message a bien été envoyé!');
            return $this->redirectToRoute('app_contact');

            }


        return $this->render('contact/index.html.twig', [
            'headers' => $headers,
            'form' => $form
        ]);
    }
}
