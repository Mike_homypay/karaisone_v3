<?php

namespace App\Controller;

use App\Service\RecuperationDeDonnees;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IrveController extends AbstractController
{
    public function __construct(
        private RecuperationDeDonnees $recuperationDeDonnees
    ) {

    }
    #[Route('/irve', name: 'app_irve')]
    public function index(): Response
    {
        $heroTexts = $this->recuperationDeDonnees->irve()['texts'];
        $cardContents = $this->recuperationDeDonnees->irve()['cards'];
        $middleTexts = $this->recuperationDeDonnees->irve()['middleTexts'];
        $middleTexts = $this->recuperationDeDonnees->irve()['middleTexts'];
        $galeries = $this->recuperationDeDonnees->gallery();

        return $this->render('irve/index.html.twig', [
            'heroTexts' => $heroTexts,
            'cardContents' => $cardContents,
            'middleTexts' => $middleTexts,
            'galeries' => $galeries

        ]);
    }
}
