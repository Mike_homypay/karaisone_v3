<?php

namespace App\Controller;

use App\Service\RecuperationDeDonnees;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class RejoignezNousController extends AbstractController
{
    public function __construct(private RecuperationDeDonnees $recuperationDeDonnees)
    {
    }
    #[Route('/rejoignez/nous', name: 'app_rejoignez_nous')]
    public function index(): Response
    {

        $homeDividers = $this->recuperationDeDonnees->homeDivider();
        $cardImages = $this->recuperationDeDonnees->rejoignezNous()['cardImages'];

        return $this->render('rejoignez_nous/index.html.twig', [
            'homeDividers' => $homeDividers,
            'cardImages' => $cardImages
        ]);
    }
}
