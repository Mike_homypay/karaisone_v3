<?php

namespace App\Controller;

use App\Repository\ServicesRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ServicesController extends AbstractController
{
    #[Route('/services', name: 'app_services')]
    public function index(ServicesRepository $services): Response
    {

        $service = $services->findAll();

        return $this->render('services/index.html.twig', [
            'services' => $service,
        ]);
    }
    #[Route('services/{id}/description', name:'app_description')]
    public function descriptionServices(ServicesRepository $services, int $id)
    {

        $description = $services->findOneBy(['id' => $id]);

        // if (!$description) {
        //     throw new NotFoundHttpException('Pas de résumer');
        // }
        return $this->render('services/descriptions.html.twig', [
            'descriptions' => $services->find($id)
        ]);
    }
}
