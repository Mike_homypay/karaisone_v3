<?php

namespace App\Controller;

use App\Service\FormService;
use App\Service\OperationService;
use App\Service\RecuperationDeDonnees;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\CommentairesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class HomeController extends AbstractController
{
    public function __construct(
        private RecuperationDeDonnees $recuperationDeDonnees,
        private FormService $formService,
        private EntityManagerInterface $entityManagerInterface,
        private CommentairesRepository $commentairesRepository,
        private OperationService $operationService,
    ) {
    }
    #[Route('/', name: 'app_home')]
    public function index(Request $request, SessionInterface $session): Response
    {
        //dd(get_class_methods($session), $session->getFlashBag() ); 
        $images = $this->recuperationDeDonnees->carouselImage();
        $cardImages = $this->recuperationDeDonnees->cardImage();
        $homeDividers = $this->recuperationDeDonnees->homeDivider();
        $partenaireImages = $this->recuperationDeDonnees->partenaire();
        $qualifications = $this->recuperationDeDonnees->qualifications();
        $zoneImplantations = $this->recuperationDeDonnees->zoneImplantation();
        $form = $this->formService->commentaires($this->entityManagerInterface, $request)['form'];
        $commentaires = $this->commentairesRepository->findAll();
        $bestCommentaires = $this->commentairesRepository->findBestCommentaires();
        $note = $this->operationService->calculNote()['note'];
        $nbCommentaires = $this->operationService->calculNote()['nbCommentaires'];

        /* Activation du formulaire et de son traitement par FormService.php */
       // $this->formService->commentaires($this->entityManagerInterface, $request);

        return $this->render('home/index.html.twig', [
            'images' => $images,
            'cardImages' => $cardImages,
            'homeDividers' => $homeDividers,
            'partenaireImages' => $partenaireImages,
            'qualifications' => $qualifications,
            'zoneImplantations' => $zoneImplantations,
            'form' => $form,
            'commentaires' => $commentaires,
            'bestCommentaires' => $bestCommentaires,
            'note' => $note,
            'nbCommentaires'  => $nbCommentaires,
        ]);
    }

    public function footer(Request $request)
    {
        $footerButtons = $this->recuperationDeDonnees->buttonFooter();
        $mentionButtons = $this->recuperationDeDonnees->mentionFooter();
        return $this->render('embed/footer.html.twig', [
            'footerButtons' => $footerButtons,
            'mentionButtons' => $mentionButtons,
            'request'  => $request
        ]);
    }

    #[Route('/mention', name: 'app_mention')]
    public function mention(Request $request)
    {
        return $this->render('rgpd/mention_legales.html.twig');
    }

    #[Route('/politiqueDeCookie', name: 'app_cookies')]
    public function cookies(Request $request)
    {
        return $this->render('rgpd/cookies.html.twig');
    }

    #[Route('/conditionUtilisation', name: 'app_conditions')]
    public function conditionUtilisation(Request $request)
    {
        return $this->render('rgpd/conditions_utilisation.html.twig');
    }

    #[Route('/confidentialite', name: 'app_confidentialite')]
    public function confidentialite(Request $request)
    {
        return $this->render('rgpd/confidentialite.html.twig');
    }
}
