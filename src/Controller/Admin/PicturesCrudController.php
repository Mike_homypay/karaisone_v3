<?php

namespace App\Controller\Admin;

use App\Entity\Albums;
use App\Entity\Pictures;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class PicturesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Pictures::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom'),
            DateTimeField::new('createdAt', 'Date de création'),
            AssociationField::new('albums', 'Albums'),
            TextField::new('imageFile', 'Image de présentation')->setFormType(VichImageType::class)->onlyOnForms(),
            ImageField::new('imageName', 'Miniatures')->setBasePath('/pictures/thumbnails')->onlyOnIndex(),

        ];
    }

}
