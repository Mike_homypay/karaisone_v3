<?php

namespace App\Controller\Admin;

use App\Entity\Albums;
use App\Entity\Pictures;
use App\Entity\Services;
use App\Entity\Sujet;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        //return parent::index();

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        return $this->render('bundles/EasyAdminBundle/layout.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Karaisone V3');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Acceuil', 'fa fa-globe', 'app_home');
        // yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Albums', 'fas fa-image', Albums::class);
        yield MenuItem::linkToCrud('Photos', 'fas fa-camera', Pictures::class);
        yield MenuItem::linkToCrud('Utilisateur', 'fas fa-user', User::class)->setPermission('ROLE_SUPER_ADMIN');
        yield MenuItem::linkToCrud('Sujets', 'fa fa-at', Sujet::class)->setPermission('ROLE_SUPER_ADMIN');
        yield MenuItem::linkToCrud('Services', 'fas fa-toolbox', Services::class)->setPermission('ROLE_SUPER_ADMIN');
    }
}
