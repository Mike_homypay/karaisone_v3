<?php

namespace App\Entity;

use App\Repository\AlbumsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AlbumsRepository::class)]
#[Vich\Uploadable]
class Albums
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(length: 255, type: 'string', nullable: true)]
    private ?string $presentationName = null;

    #[Vich\UploadableField(mapping: 'presentations', fileNameProperty: 'presentationName', size: 'presentationSize')]
    private ?File $presentationFile = null;

    #[ORM\Column(nullable: true)]
    private ?int $presentationSize = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\OneToMany(mappedBy: 'albums', targetEntity: Pictures::class)]
    private Collection $images;

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection<int, Pictures>
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Pictures $image): static
    {
        if (!$this->images->contains($image)) {
            $this->images->add($image);
            $image->setAlbums($this);
        }

        return $this;
    }

    public function removeImage(Pictures $image): static
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getAlbums() === $this) {
                $image->setAlbums(null);
            }
        }

        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $presentationFile
     */
    public function setPresentationFile(?File $presentationFile = null): void
    {
        $this->presentationFile = $presentationFile;

        if (null !== $presentationFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getPresentationFile(): ?File
    {
        return $this->presentationFile;
    }

    public function setPresentationName(?string $presentationName): void
    {
        $this->presentationName = $presentationName;
    }

    public function getPresentationName(): ?string
    {
        return $this->presentationName;
    }

    public function setPresentationSize(?int $presentationSize): void
    {
        $this->presentationSize = $presentationSize;
    }

    public function getPresentationSize(): ?int
    {
        return $this->presentationSize;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getNom();
    }
}
