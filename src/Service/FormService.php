<?php

namespace App\Service;

use DateTimeImmutable;
use App\Entity\Contact;
use App\Form\ContactType;
use App\Entity\Commentaires;
use App\Form\CommentairesType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

final class FormService extends AbstractController
{
    public function __construct(EntityManagerInterface $entityManagerInterface)
    {
    }

    /**
     * Creation du formulaire de contact
     */
    public function contact()
    {
        $contact = new Contact();

        $form = $this->createForm(ContactType::class, $contact);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact->setCreatedAt(new DateTimeImmutable('now'));
        }

        return [
            'form' => $form,
            'contact' => $contact
        ];
    }

    /**
     * Creation du formulaire de commentaires
     */
    public function commentaires(EntityManagerInterface $entityManagerInterface, Request $request)
    {
        $commentaires = new Commentaires();
        $form = $this->createForm(CommentairesType::class, $commentaires);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $commentaires->setCreatedAt(new DateTimeImmutable('now'));
            $commentaires->setIsValid(false);
            $commentaires->setNom($form->getData()->getNom());
            $commentaires->setTitre($form->getData()->getTitre());
            $commentaires->setEmail($form->getData()->getEmail());
            $commentaires->setMessage($form->getData()->getMessage());
            $entityManagerInterface->persist($commentaires);
            $entityManagerInterface->flush();
            $this->addFlash('comSuccess', $commentaires->getEmail() . ' ' . 'Votre commentaire a bien été envoyé.');

            return [
                $this->redirectToRoute('app_home', ['_fragment' => 'form']),
                'form' => $form,
                'commentaires' => $commentaires
            ];
        }

        if ($form->isSubmitted()) {
            $errors = [
                $form['nom']->getErrors(),
                $form['email']->getErrors(),
                $form['message']->getErrors(),
                $form['titre']->getErrors()
            ];
            if (count($errors) > 0) {
                $this->addFlash('errors', 'Votre commentaire n\'a pas été envoyé, veuillez verifier vos informations.');
                return [
                    $this->redirectToRoute('app_home', ['_fragment' => 'form']),
                    'form' => $form,
                    'commentaires' => $commentaires
                ];
            }
        }
        return [
            'form' => $form,
            'commentaires' => $commentaires
        ];
    }
}
