<?php

namespace App\Service;

final class RecuperationDeDonnees
{
    public function __construct(
        private array $services = [
            [
                'title' => 'Rénovation Immobilière',
                'path' => ''
            ],
            [
                'title' => 'Pose de salle de bain',
                'path' => ''
            ],
            [
                'title' => 'Plomberie',
                'path' => ''
            ],
            [
                'title' => 'Peinture',
                'path' => ''
            ],
            [
                'title' => 'Pose de cuisine',
                'path' => ''
            ],
            [
                'title' => 'Carrelage&revêtement',
                'path' => ''
            ],
            [
                'title' => 'Electricité',
                'path' => ''
            ],
            [
                'title' => 'Energie & Télécommunication',
                'path' => ''
            ],
        ],
        private $rejoignezNous = [
            [
                'title' => 'Devenir parteniare',
                'path' => ''
            ],
            [
                'title' => 'Nos offres d\'emploi',
                'path' => ''
            ]
        ],
        private $quiSommesNous = [

            'hero' => [
                'path' => 'images/quiSommesNous/hero.jpg',
                'alt' => 'Travaux de renovation'
            ],
            'historique' => [
                'path' => 'images/quiSommesNous/historique.png',
                'alt' => 'Historique de l\'entreprise Karaisone'
            ],
            'organigramme' => [
                'path' => 'images/quiSommesNous/organigramme.jpg',
                'alt' => 'Organigramme de Karaisone'
            ]
        ]
    ) {
    }

    public function carouselImage()
    {
        $images = [
            [
                "path" => "images/carousels/25bc56_3ca2b3b1ea6a472992289c200b214445~mv2.webp",
                "alt" => "Travaux de rénovation"
            ],
            [
                "path" => "images/carousels/25bc56_f97ddc8658904089bdf89776d0a16c1b~mv2.webp",
                "alt" => "Travaux de rénovation"
            ],
            [
                "path" => "images/carousels/11062b_bc75ed7edb924e29999434f9326fdf18~mv2.webp",
                "alt" => "Travaux de rénovation"
            ],
            [
                "path" => "images/carousels/11062b_db5cab9df11a4cbe8496f146f21f9310~mv2.webp",
                "alt" => "Travaux de rénovation"
            ],
            [
                "path" => "images/carousels/energie.jpg",
                "alt" => "Travaux de rénovation"
            ]
        ];
        return $images;
    }

    public function cardImage()
    {
        $images = [
            [
                "title" => "Pourquoi nous choisir ?",
                "path" => "/images/cardImages/pexels-rene-asmussen-3990359.jpg",
                "alt" => "Homme qui fait des travaux",
                "href" => "app_home",
                "texts" => [
                    "Rapport d'intervention détaillée",
                    "Proposition de solution et adaptation aux situations imprévisibles",
                    "Interlocuteur dédié à chaque dossier",
                    "Outil de gestion des interventions",
                    "Devis gratuit"
                ]
            ],
            [
                "title" => "Plan design 3D",
                "path" => "/images/cardImages/pexels-mark-2724748.jpg",
                "alt" => "Jolie cuisine rénovée",
                "href" => "app_home",
                "texts" => [
                    "Réalisation des plans en 3D pour une visualisation palpable de votre projet.",
                    "Conseil et expertise sur la faisabilité de votre projet.",
                ]
            ],
            [
                "title" => "Service de gestion et suivi des travaux",
                "path" => "/images/cardImages/pexels-nappy-935949.jpg",
                "alt" => "Suivi de",
                "href" => "app_home",
                "texts" => [
                    "Service de gestion et suivi des travaux",
                    "Il est dédié au suivi, à la bonne tenue et la réalisation de vos travaux.",
                    "Il a pour mission de coordonner votre demande à l'action de nos équipes terrains pour un rendu parfait de vos projets dans le respect des délais de traitement."
                ]
            ],
        ];

        return $images;
    }

    public function homeDivider()
    {
        $dividers = [
            [
                'font' => 'fa-solid fa-calendar',
                'title' => '2015',
                'content' => 'Année de création',
            ],
            [
                'font' => 'fa-solid fa-truck-pickup',
                'title' => '+10 000',
                'content' => 'Interventions Réalisées par an',
            ],
            [
                'font' => 'fa-solid fa-paint-roller',
                'title' => '+100',
                'content' => 'Chantiers Réalisés',
            ],
            [
                'font' => 'fa-solid fa-people-roof',
                'title' => '94 %',
                'content' => 'Clients Satifaits',
            ],
        ];

        return $dividers;
    }

    public function partenaire()
    {
        $images = [
            [
                "path" => "images/partenaire/aurore.png",
                "alt" => "Aurore"
            ],
            [
                "path" => "images/partenaire/bouygues.png",
                "alt" => "Bouygues"
            ],
            [
                "path" => "images/partenaire/castorama.png",
                "alt" => "Castorama"
            ],
            [
                "path" => "images/partenaire/coallia.png",
                "alt" => "Coallia"
            ],
            [
                "path" => "images/partenaire/enedis.webp",
                "alt" => "Enedis"
            ],
            [
                "path" => "images/partenaire/ert.webp",
                "alt" => "Ert"
            ],
            [
                "path" => "images/partenaire/oviance.png",
                "alt" => "Oviance"
            ],
            [
                "path" => "images/partenaire/sfr.png",
                "alt" => "SFR"
            ],
            [
                "path" => "images/partenaire/sogetrel.png",
                "alt" => "Sogetrel"
            ],
            [
                "path" => "images/partenaire/solution_new_tec.webp",
                "alt" => "New technologies"
            ],
            [
                "path" => "images/partenaire/voltalis.webp",
                "alt" => "Voltalis"
            ],
            [
                "path" => "images/partenaire/waat.png",
                "alt" => "Waat"
            ]
        ];
        return $images;
    }

    public function qualifications()
    {

        $images = [
            [
                'path' => 'images/footer/Qualification_4_CLR.webp',
                'alt' => 'Qualifelec'
            ],
            [
                'path' => 'images/footer/Qualification_elec.jpg',
                'alt' => 'Qualifelec'
            ],
            [
                'path' => 'images/footer/Qualification_5_CLR.webp',
                'alt' => 'Qualifelec'
            ],
        ];

        return $images;
    }

    public function zoneImplantation()
    {
        $images = [
            [
                'path' => 'images/zoneImplantation/France-departements-numeros_.webp',
                'alt' => 'Carte france zone d\'implantation'
            ]
        ];

        return $images;
    }

    public function buttonFooter()
    {
        $elements =
            [
                [
                    'texts' => 'Accueil',
                    'href' => 'app_home'
                ],
                [
                    'texts' => 'Qui sommes-nous?',
                    'href' => 'app_qui_sommes_nous'
                ],
                [
                    'texts' => 'Services',
                    'href' => 'app_services',
                    'dropdown' => $this->services,
                    'id' => 'services'

                ],
                [
                    'texts' => 'IRVE',
                    'href' => 'app_irve'
                ],
                [
                    'texts' => 'Réalisations',
                    'href' => 'app_realisations'
                ],
                [
                    'texts' => 'Rejoignez-nous',
                    'href' => 'app_rejoignez_nous'
                ],
                [
                    'texts' => 'Contact',
                    'href' => 'app_contact'
                ],
            ];



        return $elements;
    }

    public function qui()
    {
        $images = $this->quiSommesNous;

        return $images;
    }

    public function irve()
    {
        $heroTexts = [
            'title' => 'DÉPLOIEMENT ET INSTALLATION DE BORNE DE RECHARGE POUR VÉHICULES ÉLECTRIQUE AU SERVICE DES PARTICULIERS ET PROFESSIONNELS :',
            'p' => [
                'PARTICULIERS : maison individuelle',
                ' PROFESSIONNELS : entreprises privées, collectivités, copropriétés, parking, hôpitaux, CHR, centres commerciaux, garages...',
                'Étude d\'implantation, réalisation de schéma 3D, installation et maintenance, accompagnement en matière d\'obtention de prime d\'installation.',
                'Nous nous spécialisons dans l\'installation d\'infrastructures de recharge de véhicules électriques auprès des particuliers, professionnels, collectivités, copropriétés, hôpitaux... Dans le but de prôner une consommation énergétique écoresponsable, la tendance se tourne de plus en plus vers les véhicules électriques. Pour ce faire, il est important de disposer d\'un système de recharge fiable et efficace.'
            ]
        ];

        $middleTexts = [
            'p' => [
                "Vous avez un projet d'installation d'une infrastructure de recharge pour véhicules électriques (IRVE), vous avez un projet de déploiement de borne de recharge pour favoriser la mobilité électrique. Vous avez frappé à la bonne porte, contactez notre équipe dès aujourd'hui. Nous vous aidons à franchir la prochaine étape vers une mobilité électrique en vous livrant clé en main votre solution de recharge électrique adaptée à votre besoin."
            ]
        ];

        $cardContents = [
            [
                'title' => 'Consultation et conception',
                'texts' => [
                    'Compréhension de votre besoin et objectifs spécifiques',
                    'Proposition d\'une solution de recharge personnalisée répondant au cahier de charges avec projection de plan 3D', 'Nous prenons en compte le nombre de véhicules que vous devez recharger, l\'espace disponible et votre budget'
                ],
                'font' => 'pen-ruler'
            ],
            [
                'title' => 'Installation et Configuration',
                'texts' => [
                    'Définition du projet',
                    'Proposition d\'une solution de recharge personnalisée répondant au cahier de charges avec projection de plan 3D',
                ],
                'font' => 'pen-to-square'
            ],
            [
                'title' => 'Maintenance et soutien',
                'texts' => [
                    'Disponibilité des équipes pour effectuer une maintenance sur votre point de charge',
                    'Soutien à distance pour toute question quant au fonctionnement de la borne de recharge',
                ],
                'font' => 'screwdriver-wrench'
            ],
            [
                'title' => 'Mise à niveau et expansion',
                'texts' => [
                    'A mesure que vos besoins évoluent , nous pouvons mettre à niveau ou agrandir votre système de recharge pour plus de bornes ou une solution de recharge plus rapide.',
                ],
                'font' => 'charging-station'
            ]
        ];

        return [
            'texts' => $heroTexts,
            'cards' => $cardContents,
            'middleTexts' => $middleTexts
        ];
    }

    public function gallery()
    {
        $images = [
            [
                'path' => 'images/irve/station.webp',
                'content' => 'Borne de recharge murale'
            ],
            [
                'path' => 'images/irve/chargeur_rapide.webp',
                'content' => 'Chargeur rapide'
            ],
            [
                'path' => 'images/irve/borne.webp',
                'content' => 'Borne de recharge sur pied'
            ],
        ];

        return $images;
    }

    public function contact()
    {
        $headers = [
            [
                'font' => 'location-dot', 'text' => '7, Impasse des chasses marées 95610 Éragny, France'
            ],  [
                'font' => 'envelope-open-text', 'text' => 'contact@karaisone.fr'
            ],  [
                'font' => 'phone', 'text' => '+33 1 83 64 44 67</br>+33 6 09 99 32 08'
            ],
        ];

        return ['headers' => $headers];
    }

    public function rejoignezNous()
    {
        $cards = [
            [
                'path' => 'images/rejoignezNous/salle_de_bain.jpg',
                'text' => 'Salle de bain moderne'
            ],
            [
                'path' => 'images/rejoignezNous/cuisine.jpg',
                'text' => 'Cuisine blanche'
            ],
            [
                'path' => 'images/rejoignezNous/parquet.jpg',
                'text' => 'Installation de parquet flotant'
            ],
            [
                'path' => 'images/rejoignezNous/circuit_electrique.jpg',
                'text' => 'Circuit électrique'
            ],
        ];

        return ['cardImages' => $cards];
    }
    public function mentionFooter()
    {
        $elements =
            [
                [
                    'texts' => 'Mentions légales',
                    'href' => 'app_mention'
                ],
                [
                    'texts' => 'Politique en matière de cookies',
                    'href' => 'app_cookies'
                ],
                [
                    'texts' => 'politique de confidentialité',
                    'href' => 'app_confidentialite',
                ],
                [
                    'texts' => "Condition d'utilisation",
                    'href' => 'app_conditions'
                ]
            ];



        return $elements;
    }
}
