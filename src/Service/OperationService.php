<?php

namespace App\Service;

use App\Repository\CommentairesRepository;
use Symfony\Component\Validator\Constraints\Length;

final class OperationService
{
    public function __construct(
        private CommentairesRepository $commentairesRepository
    ) {
        $this->commentairesRepository = $commentairesRepository;
    }
    /**
     * Calcul de la note //satisfaction client
     * 
     */
    public function calculNote()
    {
        $note = 0;
        /* Addition de l'ensemble des notes */
        foreach ($this->commentairesRepository->findAll() as $commentaire) {
            $note += $commentaire->getNote();
        }
        // Nombre de commentaires
        $nbCommentaires = count($this->commentairesRepository->findAll());
    
        /* Calcul de la note moyenne */
        $noteGlobal = "";
        if ($note == 0) {
            "pas de note";
        } else {
            $noteGlobal = $note / $nbCommentaires;
        }
        
        
        return ['note' => $noteGlobal, 'nbCommentaires' => $nbCommentaires];
    }
}
