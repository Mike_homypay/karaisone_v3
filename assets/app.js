import './bootstrap.js';

import './styles/app.scss';

import 'tw-elements';

import "@fortawesome/fontawesome-free/css/all.css";
import "@fortawesome/fontawesome-free/js/all";


console.log('This log comes from assets/app.js - welcome to AssetMapper! 🎉');

import {
    Carousel,
    Ripple,
    initTE,  
    Input,
    Collapse,
    Dropdown, 
    Select,
    Modal
  } from "tw-elements";
  
  initTE({ Carousel, Ripple, Dropdown, Input, Collapse, Select, Modal
  });
  
  window.axeptioSettings = {
    clientId: "664c6b74fb9e60409919c0be",
    
  };
  (function (d, s) {
    var t = d.getElementsByTagName(s)[0],
      e = d.createElement(s);
    e.async = true;
    e.src = "//static.axept.io/sdk.js";
    t.parentNode.insertBefore(e, t);
  })(document, "script");
