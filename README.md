# Karaisone

- PHP 8.2.14 (cli) (built: Dec 21 2023 20:19:50) (NTS)
    Copyright (c) The PHP Group
    Zend Engine v4.2.14, Copyright (c) Zend Technologies
    with Zend OPcache v8.2.14, Copyright (c), by Zend Technologies
- Symfony 7.0.2 (env: dev, debug: true) 


## Coding Standard

<https://symfony.com/doc/current/contributing/code/standards.html#symfony-coding-standards-in-detail>

` tools/php-cs-fixer/vendor/bin/php-cs-fixer fix src`

### test
https://codepen.io/oidre/pen/vYGBaVZ?editors=1000

pour relancer le yarn encore Dev en cas de probleme supprimer le fichier yarn.lock et le dossier nodes_modules puis lancer une command yarn